from .app import db

class Author(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(100))

    def __repr__(self):
        return "<Author (%d) %s>" % (self.id, self.name)

def get_author(id):
    return Author.query.filter(lambda id: self.id == id, Author.query.all())

class Album(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    title = db.Column(db.String(100))
    title_groupe = db.Column(db.String(100))
    annee = db.Column(db.Integer)
    img = db.Column(db.String(100))
    author_id = db.Column(db.Integer, db.ForeignKey("author.id"))
    genre_id = db.Column(db.Integer, db.ForeignKey("genre.id"))

    def __repr__(self):
        return "<Album (%d) %s" % (self.id, self.title, self.title_groupe, self.annee, self.img, self.author_id, self.genre_id)

class Genre(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(100))

    def __repr__(self):
        return "<Genre (%d) %s>" % (self.id, self.name)

def get_sample():
    return Album.query.limit(50).all()

from flask.ext.login import UserMixin

class User(db.Model, UserMixin):
    username = db.Column(db.String(50), primary_key=True)
    password = db.Column(db.String(64))

    def get_id(self):
        return self.username

from .app import login_manager

@login_manager.user_loader
def load_user(username):
    return User.query.get(username)
