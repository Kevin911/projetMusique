from .app import app, db
from flask import render_template, url_for, redirect, request
from .models import get_sample, Author, User, Album, Genre
from flask.ext.wtf import Form
from wtforms import StringField, HiddenField, PasswordField, IntegerField
from wtforms.validators import DataRequired
from hashlib import sha256
from flask.ext.login import login_user, current_user, logout_user, login_required

@app.route("/")
def home():
    return render_template("home.html",title="Liste d'Albums!",albums=get_sample())

class Authorform(Form):
    id = HiddenField('id')
    name = StringField('Nom', validators=[DataRequired()])

def get_author(id):
    return Author.query.get(id);

def get_album(id):
    return Album.query.get(id);

@app.route("/author/<int:id>")
def one_author(id):
    a= get_author(id)
    f=Authorform(id=a.id, name=a.name)
    return render_template("author.html",author=a, form=f)

@app.route("/edit/author/<int:id>")
@login_required
def edit_author(id):
    a = get_author(id)
    f = Authorform(id=a.id, name=a.name)
    return render_template("edit-author.html",author=a, form=f)

@app.route("/save/author/", methods=("POST",))
def save_author():
    a=None
    f=Authorform()
    if f.validate_on_submit():
        id = int(f.id.data)
        a = get_author(id)
        a.name=f.name.data
        db.session.commit()
        return redirect(url_for('one_author', id=a.id))
    a= get_author(int(f.id.data))
    return render_template("edit-author.html",author=a, form=f)


class LoginForm(Form):
    username = StringField('Username')
    password = PasswordField('Password')
    next = HiddenField()

    def get_authenticated_user(self):
        user = User.query.get(self.username.data)
        if user is None:
            return None
        m=sha256()
        m.update(self.password.data.encode())
        passwd = m.hexdigest()
        return user if passwd == user.password else None

@app.route("/login/", methods=("GET","POST"))
def login():
    f = LoginForm()
    if not f.is_submitted():
        f.next.data = request.args.get("next")
    elif f.validate_on_submit():
        user = f.get_authenticated_user()
        if user:
            login_user(user)
            next = f.next.data or url_for("home")
            return redirect(next)
    return render_template("login.html", form=f)

@app.route("/login/")
def logout():
    logout_user()
    return redirect(url_for('home'))

class Authorformajout(Form):
    id = None
    name = StringField('Nom', validators=[DataRequired()])

@app.route("/ajout/author/")
@login_required
def ajout_author():
    f = Authorformajout(id=None, name=None)
    return render_template("ajout-author.html",form=f)

@app.route("/save/ajout/author/", methods=("POST",))
def save_author_ajout():
    from sqlalchemy import func
    f=Authorformajout()
    if f.validate_on_submit():
        u = db.session.query(func.max(Author.id)).scalar()+1
        a=Author(id=u, name=f.name.data)
        db.session.add(a)
        db.session.commit()
        return redirect(url_for('one_author', id=a.id))
    return render_template("ajout-author.html",form=f)

@app.route("/supprimer/author/<int:id>")
def suppr_author(id):
    Author.query.filter_by(id=id).delete()
    db.session.commit()
    return redirect(url_for('home'))

# ajout, edit d'un album

class Albumformajout(Form):
    id = None
    title = StringField('Titre', validators=[DataRequired()])
    title_groupe = StringField('Titre Groupe', validators=[DataRequired()])
    annee = IntegerField('Annee', validators=[DataRequired()])
    img = StringField('Image', validators=[DataRequired()])
    author_id = IntegerField('ID Author', validators=[DataRequired()])
    genre_id = IntegerField('ID Genre', validators=[DataRequired()])

@app.route("/ajout/album/")
@login_required
def ajout_album():
    f = Albumformajout(id=None, title=None, title_groupe=None, annee=None, img=None, author_id=None, genre_id=None)
    return render_template("ajout_album.html",form=f)

@app.route("/save/ajout/album/", methods=("POST",))
def save_album_ajout():
    from sqlalchemy import func
    f=Albumformajout()
    if f.validate_on_submit():
        u = db.session.query(func.max(Album.id)).scalar()+1
        a=Album(id=u, title=f.title.data, title_groupe=f.title_groupe.data, annee=f.annee.data, img=f.img.data, author_id=f.author_id.data, genre_id=f.genre_id.data)
        db.session.add(a)
        db.session.commit()
        return redirect(url_for('one_album', id=a.id))
    return render_template("ajout_album.html",form=f)

@app.route("/supprimer/album/<int:id>")
def suppr_album(id):
    Album.query.filter_by(id=id).delete()
    db.session.commit()
    return redirect(url_for('home'))


@app.route("/album/<int:id>")
def one_album(id):
    a= get_album(id)
    f=Albumformajout(id=a.id, title=a.title, title_groupe=a.title_groupe, annee=a.annee, img=a.img, author_id=Author.query.filter_by(id=a.author_id).first().name, genre_id=Genre.query.filter_by(id=a.genre_id).first().name)
    return render_template("album.html",album=a, form=f)

@app.route("/edit/album/<int:id>")
@login_required
def edit_album(id):
    a = get_album(id)
    f=Albumformajout(id=a.id, title=a.title, title_groupe=a.title_groupe, annee=a.annee, img=a.img, author_id=a.author_id, genre_id=a.genre_id)
    return render_template("edit_album.html",album=a, form=f)

# recherche par année
class anneeform(Form):
    annee = IntegerField('Annee', validators=[DataRequired()])

def get_album_annee(annee):
    return Album.query.filter_by(annee=annee);

@app.route("/selection/annee/")
def select_annee():
    f = anneeform(annee=None)
    return render_template("select_annee.html",form=f)

@app.route("/recherche/par_annee/", methods=("POST",))
def one_annee():
    f=anneeform()
    a = get_album_annee(f.annee.data)
    return render_template("par_annee.html",album=a, form=f)

# recherche par auteur
class authorform(Form):
    name = StringField('Nom', validators=[DataRequired()])

def get_album_author(name):
    x = Author.query.filter_by(name=name).first()
    return Album.query.filter_by(author_id=x.id);

@app.route("/selection/author/")
def select_author():
    f = authorform(name=None)
    return render_template("select_author.html",form=f)

@app.route("/recherche/par_author/", methods=("POST",))
def one_par_author():
    f=authorform()
    a = get_album_author(f.name.data)
    return render_template("par_author.html",author=a, form=f)

# recherche par genre
class genreform(Form):
    name = StringField('Nom', validators=[DataRequired()])

def get_album_genre(name):
    x = Genre.query.filter_by(name=name).first()
    return Album.query.filter_by(genre_id=x.id);

@app.route("/selection/genre/")
def select_genre():
    f = genreform(name=None)
    return render_template("select_genre.html",form=f)

@app.route("/recherche/par_genre/", methods=("POST",))
def one_par_genre():
    f=genreform()
    a = get_album_genre(f.name.data)
    return render_template("par_genre.html",genre=a, form=f)

# recherche par titre
class titleform(Form):
    title = StringField('Titre', validators=[DataRequired()])

def get_album_title(title):
    return Album.query.filter_by(title=title);

@app.route("/selection/titre/")
def select_title():
    f = titleform(title=None)
    return render_template("select_titre.html",form=f)

@app.route("/recherche/par_titre/", methods=("POST",))
def one_title():
    f=titleform()
    a = get_album_title(f.title.data)
    return render_template("par_titre.html",album=a, form=f)

# trie liste auteurs
class formlisteauteur(Form):
    recherche = StringField(validators=[DataRequired()])

def getlisteauteur(recherche):
    if recherche.data == None:
        return Author.query.order_by(Author.name.asc()).all()
    else:
        return Author.query.filter(Author.name.like(recherche.data + '%')).order_by(Author.name.asc()).all()

@app.route("/liste/auteur")
def one_listeauteur():
    f = formlisteauteur()
    a = getlisteauteur(f.recherche)
    return render_template("home_author.html",author=a, form=f, title="Liste d'Auteurs!")

@app.route("/liste/auteur", methods=("POST",))
def one_listeauteurs():
    f = formlisteauteur()
    a = getlisteauteur(f.recherche)
    return render_template("home_author.html",author=a, form=f, title="Liste d'Auteurs!")

# trie liste genres
class formlistegenre(Form):
    recherche = StringField(validators=[DataRequired()])

def getlistegenre(recherche):
    if recherche.data == None:
        return Genre.query.order_by(Genre.name.asc()).all()
    else:
        return Genre.query.filter(Genre.name.like(recherche.data + '%')).order_by(Genre.name.asc()).all()

@app.route("/liste/genre")
def one_listegenre():
    f = formlistegenre()
    a = getlistegenre(f.recherche)
    return render_template("home_genre.html",genre=a, form=f, title="Liste des Genres!")

@app.route("/liste/genre", methods=("POST",))
def one_listegenres():
    f = formlistegenre()
    a = getlistegenre(f.recherche)
    return render_template("home_genre.html",genre=a, form=f, title="Liste des Genres!")
