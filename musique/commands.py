from .app import manager, db

@manager.command
def loaddb(filename):
    '''Creates the tables and populates them with data.'''

    #création de toutes les tables
    db.create_all()

    #chargement de notre jeu de données
    import yaml
    albums = yaml.load(open(filename))

    #import des modèles
    from .models import Author, Album, Genre

    # première passe: création de tous les auteurs
    authors = {}
    for a in albums:
        b = a["by"]
        if b not in authors:
            o = Author(name=b)
            db.session.add(o)
            authors[b] = o
    db.session.commit()

    #deuxième passe: création de tous les genres
    genres = {}
    for a in albums:
        for x in a["genre"]:
            b = x
        if b not in genres:
            o = Genre(name=b)
            db.session.add(o)
            genres[b] = o
    db.session.commit()

    # troisième passe: création de tous les albums
    for b in albums:
        a = authors[b["by"]]
        for key in genres:
            if genres[key].name in b["genre"]:
                #print(genres[key].name)
                g = genres[key].id
        o = Album(title = b["title"],title_groupe = b["parent"],annee=b["releaseYear"],img=b["img"],author_id=a.id,genre_id=g)
        db.session.add(o)
    db.session.commit()

@manager.command
def syncdb():
    db.create_all()

@manager.command
def newuser(username, password):
    from .models import User
    from hashlib import sha256
    m=sha256()
    m.update(password.encode())
    u=User(username=username, password=m.hexdigest())
    db.session.add(u)
    db.session.commit()

@manager.command
def passwd(username, password):
    from .models import User
    from hashlib import sha256
    m=sha256()
    m.update(password.encode())
    u=User.query.filter_by(username=username).first()
    u.password=m.hexdigest()
    db.session.commit()
